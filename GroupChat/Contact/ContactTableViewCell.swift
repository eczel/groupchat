//
//  ContactTableViewCell.swift
//  GroupChat
//
//  Created by Elveen on 16/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import UIKit

protocol ContactTableViewCellDelegate
{
    func didtapCheckbox(indexNo : Int, name: String, select: Bool)
}

class ContactTableViewCell: UITableViewCell
{
    @IBOutlet weak var contactImage : UIImageView!
    @IBOutlet weak var contactName : UILabel!
    @IBOutlet weak var checkboxBtn : UIButton!
    
    var contactDelegate : ContactTableViewCellDelegate?
    var indexNo : IndexPath?
    var name : String?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        checkboxBtn.imageView?.contentMode = .scaleAspectFit
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func tappedCheckbox(_ sender: UIButton)
    {
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveLinear, animations:
        {
            sender.isSelected = !sender.isSelected
            if self.checkboxBtn.isSelected == true
            {
                print(self.indexNo!.row)
                print("select")
                self.contactDelegate?.didtapCheckbox(indexNo: self.indexNo!.row, name: self.name ?? "", select: true)
            
            }else
            {
                print(self.indexNo!.row)
                sender.transform = .identity
                print("unselect")
                self.contactDelegate?.didtapCheckbox(indexNo: self.indexNo!.row, name: self.name ?? "", select: false)
            }
        })
    }
}











//        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveLinear, animations:
//        {
//            if !sender.isSelected
//            {
//
//                print("selected")
//                self.contactDelegate?.didtapCheckbox(select: true)
//
//            }else
//            {
//                print("unselect")
//                self.contactDelegate?.didtapCheckbox(select: false)
//            }
//        }, completion: nil)






//        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveLinear, animations:
//        {
//            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
//            print("selected")
//        })
//        {(success) in
//            sender.isSelected = !sender.isSelected
//            UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveLinear, animations:
//            {
//                sender.transform = .identity
//                print("unselect")
//            }, completion: nil)
//        }


