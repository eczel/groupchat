//
//  TableContact.swift
//  GroupChat
//
//  Created by Elveen on 16/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import Foundation
import UIKit

//------------------------protocol getNewContact-------------------------------
//{
//    func didUpdateContact(for cell: UITableViewCell)
//}


//------------------------------send Data using protocol and segue-------------------------
//protocol SendData
//{
//    func sendDataToNewVc() -> [AnyObject]
//}
//-----------------------------------------------------------------------------------------

struct listcontact
{
    var indexNo : Int?
    var name : String?
    var select : Bool?
}

class Contact : UIViewController
{
    @IBOutlet weak var tableContact : UITableView!
    @IBOutlet weak var confirmBtn: UIButton!
    
    //var listContacts = [contactData]()
    var listContactData = [(listcontact(indexNo: 0, name: "user1", select: false)),
                           (listcontact(indexNo: 1, name: "user2", select: false)),
                           (listcontact(indexNo: 2, name: "user3", select: false)),
                           (listcontact(indexNo: 3, name: "user4", select: false)),
                           (listcontact(indexNo: 4, name: "user5", select: false))]
    
    
    
    let pImage = UIImage(named: "ProfileImage")
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableContact.delegate = self
        tableContact.dataSource = self
        tableContact.reloadData()
        
        btnProperties()
    }
    
    func btnProperties()
    {
        confirmBtn.layer.cornerRadius = 16
        confirmBtn.layer.shadowOpacity = 0.25
        confirmBtn.layer.shadowRadius = 5
        confirmBtn.layer.shadowOffset = CGSize(width: 0, height: 5)
    }
    
    
//    func newArray()
//    {
//        for x in self.listContact{
//            let handler = contactData()
//            x.indexNo = handler.indexNo
//        }
//    }


    
//-----------------------Change viewcontroller programmatically------------------------------
//    @IBAction func TappedConfirm(_ sender: Any)
//    {
//         print(listContactData)
//
//
//        listContactData = listContactData.filter{ $0.select(true) }
//        let groupChatVC = storyboard?.instantiateViewController(withIdentifier: "groupChat")
//        self.present(groupChatVC!, animated: true, completion: nil)
//
//    }
//--------------------------------------------------------------------------------------------
    
    
//------------------------Send Data to GroupChat using segue----------------------------------
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let destVC = segue.destination as? TableGroupChat
        {
            //destVC.getContact = listContactData.filter{$0.select == true} as [AnyObject]
            destVC.getContact = listContactData.filter{$0.select == true}
        }
    }
//--------------------------------------------------------------------------------------------

    
//------------------------Send Data to SlideMenu using segue----------------------------------
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
//    {
//        if let otherVC = segue.destination as? SlideMenu
//        {
//            otherVC.titleMenu = listContactData.filter{$0.select == true} as [AnyObject]
//        }
//    }
//--------------------------------------------------------------------------------------------
}

extension Contact: ContactTableViewCellDelegate
{
    func didtapCheckbox(indexNo: Int, name: String, select: Bool)
    {
        if listContactData.index(where: {$0.indexNo == indexNo}) != nil
        {
            guard let contactDat = listContactData.index(where: {$0.indexNo == indexNo})
                else {return}
            self.listContactData[contactDat].select = select
        }
    }
}

extension Contact : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return listContactData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactTableViewCell
        cell.contactDelegate = self
        cell.indexNo = indexPath
        cell.contactName.text = listContactData[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
}


//------------------------------send Data using protocol and segue---------------------------
//extension Contact : SendData
//{
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
//    {
//        if let destVC = segue.destination as? TableGroupChat
//        {
//            destVC.senderDelegate = self
//        }
//    }
//
//    func sendDataToNewVc() -> [AnyObject]
//    {
//        return self.listContactData as [AnyObject]
//    }
//}
//-------------------------------------------------------------------------------------------



