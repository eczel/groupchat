//
//  ChatBubbleLabel.swift
//  GroupChat
//
//  Created by Elveen on 09/05/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import Foundation
import UIKit

class ChatBubbleLabel: UILabel
{
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func ChangeTextColor(NewTextColor: UIColor)
    {
        self.textColor = NewTextColor
    }
    
    func showIncomingMessage(text: String)
    {
        self.numberOfLines = 0
        self.font = UIFont.systemFont(ofSize: 18)
        self.textColor = .black
        self.text = text
        
        let constraintRect = CGSize(width: 0.66 * self.frame.width,
                                    height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [.font: self.font],
                                            context: nil)
        self.frame.size = CGSize(width: ceil(boundingBox.width),
                                  height: ceil(boundingBox.height))
        
        let bubbleSize = CGSize(width: self.frame.width + 28,
                                height: self.frame.height + 20)
        
        self.frame.size = bubbleSize
    }
}
