//
//  DiscoverTextView.swift
//  GroupChat
//
//  Created by Elveen on 07/05/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import Foundation
import UIKit

class DiscoverTextView: UITextView
{
    override init(frame: CGRect, textContainer : NSTextContainer?)
    {
        super.init(frame: frame, textContainer : textContainer)
        setupTextView()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setupTextView()
    }
    
    func setupTextView()
    {
        textProperties()
    }
    
    func textProperties()
    {
        translatesAutoresizingMaskIntoConstraints = false
        contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        sizeToFit()
        let lightestGrayColor: UIColor = UIColor.lightGray
        layer.borderColor = lightestGrayColor.cgColor
        layer.borderWidth = 0.6
        layer.cornerRadius = 6.0
        clipsToBounds = true
        layer.masksToBounds = true
        isScrollEnabled = true
        delegate = self
    }
    
    private var placeholderLabel = UILabel()
    
    func placeholder(editText: String)
    {
        placeholderLabel.text = editText
        placeholderLabel.font = UIFont.italicSystemFont(ofSize: (self.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (self.font?.pointSize)!/2)
        placeholderLabel.textColor = UIColor.lightGray
        
        self.addSubview(placeholderLabel)
        delegate = self
    }
    
    //    private var placeholderEnable: Bool = true
    func placeholderState(placeholderEnable: Bool)
    {
        if placeholderEnable == false
        {
            placeholderLabel.isHidden = true
        }else
        {
            placeholderLabel.isHidden = false
        }
    }
}

extension DiscoverTextView : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        placeholderLabel.isHidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (self.text == "")
        {
            placeholderLabel.isHidden = false
        }
    }
}

