//
//  ChatBubbleView.swift
//  GroupChat
//
//  Created by Elveen on 08/05/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import Foundation
import UIKit

class chatBubbleView: UIView
{
    var isIncoming = false
    
    var incomingColor = UIColor(white: 0.9, alpha: 1)
    var outgoingColor = UIColor(red: 0.09, green: 0.54, blue: 1, alpha: 1)
    
    func ChangeIncomingColor(ChangeINColor : UIColor)
    {
        incomingColor = ChangeINColor
    }
    
    func ChangeOutgoingColor(ChangeOUTColor : UIColor)
    {
        outgoingColor = ChangeOUTColor
    }
    
//    func ShadowProperties()
//    {
//        self.layer.shadowColor = UIColor.black.cgColor
//        self.layer.shadowOpacity = 0.3
//        self.layer.shouldRasterize = true
//        self.layer.rasterizationScale = UIScreen.main.scale
//    }
    
    func createShadowLayer() -> CALayer {
        let shadowLayer = CALayer()
        shadowLayer.shadowColor = UIColor.black.cgColor
        shadowLayer.shadowOffset = CGSize.zero
        shadowLayer.shadowRadius = 5.0
        shadowLayer.shadowOpacity = 0.3
        shadowLayer.backgroundColor = UIColor.clear.cgColor
        return shadowLayer
    }
    
    override func draw(_ rect: CGRect)
    {
        let width = rect.width
        let height = rect.height
        
        let bezierPath = UIBezierPath()
        let line = CAShapeLayer()
    
        if isIncoming
        {
            bezierPath.move(to: CGPoint(x: 22, y: height))
            bezierPath.addLine(to: CGPoint(x: width - 17, y: height))
            bezierPath.addCurve(to: CGPoint(x: width, y: height - 17), controlPoint1: CGPoint(x: width - 7.61, y: height), controlPoint2: CGPoint(x: width, y: height - 7.61))
            bezierPath.addLine(to: CGPoint(x: width, y: 17))
            
            //--------------------shadowlayer------------
            line.path = bezierPath.cgPath
            line.strokeColor = UIColor.black.cgColor
            line.fillColor = UIColor.clear.cgColor
            line.lineWidth = 2.0
            line.opacity = 0.2
            self.layer.addSublayer(line)
            
            let shadowSubLayer = createShadowLayer()
            shadowSubLayer.insertSublayer(line, at: 0)
            self.layer.addSublayer(shadowSubLayer)
            //-------------------------------------------
            
            bezierPath.addCurve(to: CGPoint(x: width - 17, y: 0), controlPoint1: CGPoint(x: width, y: 7.61), controlPoint2: CGPoint(x: width - 7.61, y: 0))
            bezierPath.addLine(to: CGPoint(x: 21, y: 0))
            bezierPath.addCurve(to: CGPoint(x: 4, y: 17), controlPoint1: CGPoint(x: 11.61, y: 0), controlPoint2: CGPoint(x: 4, y: 7.61))
            bezierPath.addLine(to: CGPoint(x: 4, y: height - 11))
            bezierPath.addCurve(to: CGPoint(x: 0, y: height), controlPoint1: CGPoint(x: 4, y: height - 1), controlPoint2: CGPoint(x: 0, y: height))
            bezierPath.addLine(to: CGPoint(x: -0.05, y: height - 0.01))
            bezierPath.addCurve(to: CGPoint(x: 11.04, y: height - 4.04), controlPoint1: CGPoint(x: 4.07, y: height + 0.43), controlPoint2: CGPoint(x: 8.16, y: height - 1.06))
            bezierPath.addCurve(to: CGPoint(x: 22, y: height), controlPoint1: CGPoint(x: 16, y: height), controlPoint2: CGPoint(x: 19, y: height))
            
            incomingColor.setFill()

        }
        else
        {
            bezierPath.move(to: CGPoint(x: width - 22, y: height))
            bezierPath.addLine(to: CGPoint(x: 17, y: height))
            bezierPath.addCurve(to: CGPoint(x: 0, y: height - 17), controlPoint1: CGPoint(x: 7.61, y: height), controlPoint2: CGPoint(x: 0, y: height - 7.61))
            bezierPath.addLine(to: CGPoint(x: 0, y: 17))
            
            //----------------shadowlayer--------------
            line.path = bezierPath.cgPath
            line.strokeColor = UIColor.black.cgColor
            line.fillColor = UIColor.clear.cgColor
            line.lineWidth = 2.0
            line.opacity = 0.2
            self.layer.addSublayer(line)
            
            let shadowSubLayer = createShadowLayer()
            shadowSubLayer.insertSublayer(line, at: 0)
            self.layer.addSublayer(shadowSubLayer)
            //-----------------------------------------
            
            bezierPath.addCurve(to: CGPoint(x: 17, y: 0), controlPoint1: CGPoint(x: 0, y: 7.61), controlPoint2: CGPoint(x: 7.61, y: 0))
            bezierPath.addLine(to: CGPoint(x: width - 21, y: 0))
            bezierPath.addCurve(to: CGPoint(x: width - 4, y: 17), controlPoint1: CGPoint(x: width - 11.61, y: 0), controlPoint2: CGPoint(x: width - 4, y: 7.61))
            bezierPath.addLine(to: CGPoint(x: width - 4, y: height - 11))
            bezierPath.addCurve(to: CGPoint(x: width, y: height), controlPoint1: CGPoint(x: width - 4, y: height - 1), controlPoint2: CGPoint(x: width, y: height))
            bezierPath.addLine(to: CGPoint(x: width + 0.05, y: height - 0.01))
            bezierPath.addCurve(to: CGPoint(x: width - 11.04, y: height - 4.04), controlPoint1: CGPoint(x: width - 4.07, y: height + 0.43), controlPoint2: CGPoint(x: width - 8.16, y: height - 1.06))
            bezierPath.addCurve(to: CGPoint(x: width - 22, y: height), controlPoint1: CGPoint(x: width - 16, y: height), controlPoint2: CGPoint(x: width - 19, y: height))
            
            outgoingColor.setFill()
        }
        
        bezierPath.close()
        bezierPath.fill()
    }
}
