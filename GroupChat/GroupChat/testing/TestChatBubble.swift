//
//  TestChatBubble.swift
//  GroupChat
//
//  Created by Elveen on 08/05/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import Foundation
import UIKit

class testBubble: UIViewController
{
    @IBOutlet weak var testView: chatBubbleView!
    
    @IBOutlet weak var testLabel: ChatBubbleLabel!
    @IBOutlet weak var testView2: chatBubbleView!
    
    @IBOutlet weak var View2LeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var View2RightConstraint: NSLayoutConstraint!
    
    var leftConstraint: NSLayoutConstraint!
    var allignRightConstraint: NSLayoutConstraint!
    var rightConstraint: NSLayoutConstraint!
    var allignLeftConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        testView.isIncoming = true
//        testView.showIncomingMessage(text: "test incoming message, test outgoing message.")
        
        //------ set text
        testLabel.showIncomingMessage(text: "test incoming message, tested asdad sadad asda dasadasda asdad sadadadsa asda dadasda sdad adadas sasdad ")
        
        //------ change text colour. default are black
        testLabel.ChangeTextColor(NewTextColor: .blue)
        
        //------ change incoming and outgoing chatbubble.   default are false
        testView2.isIncoming = false
        
        //------ change colour of chatbubble.   default are lightGray and blue
        testView2.ChangeIncomingColor(ChangeINColor: UIColor.brown)
        testView2.ChangeOutgoingColor(ChangeOUTColor: UIColor.yellow)
        
        //------ shadow
        //testView2.ShadowProperties()
        testView2.createShadowLayer()

        
        //--------------------constraint correction------------------------------
        leftConstraint = NSLayoutConstraint (item: testView2, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 16)
        
        allignRightConstraint = NSLayoutConstraint (item: testView2, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -16)
        
        
        rightConstraint = NSLayoutConstraint (item: testView2, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.lessThanOrEqual, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 16)
        
        allignLeftConstraint = NSLayoutConstraint (item: testView2, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 16)
        
        if testView2.isIncoming == true
        {
            IncomingAllignmentCorrection()
        }else
        {
            OutgoingAllignmentCorrection()
        }
    }
    
    func IncomingAllignmentCorrection()
    {
        testLabel.textAlignment = .left
        
        view.removeConstraint(View2LeftConstraint)
        view.addConstraint(allignLeftConstraint)
        
        view.removeConstraint(View2RightConstraint)
        view.addConstraint(rightConstraint)
        
        view.setNeedsLayout()
    }
    
    func OutgoingAllignmentCorrection()
    {
        testLabel.textAlignment = .right
        
        view.removeConstraint(View2LeftConstraint)
        view.addConstraint(leftConstraint)
        
        view.removeConstraint(View2RightConstraint)
        view.addConstraint(allignRightConstraint)
        
        view.setNeedsLayout()
    }
}
