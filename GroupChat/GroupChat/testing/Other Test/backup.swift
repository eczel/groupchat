//
//  backup.swift
//  GroupChat
//
//  Created by Elveen on 02/05/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//





//
//
//class TableGroupChat: UIViewController
//{
//    @IBOutlet weak var tableChat: UITableView!
//    @IBOutlet weak var menuTableView: UITableView!
//    @IBOutlet weak var textViewChat: UITextView!
//    @IBOutlet weak var textViewPlaceholder: UIView!
//    @IBOutlet weak var sendMessageBtn: UIButton!
//    @IBOutlet var SlideMenu: UIView!
//    @IBOutlet var dropMenu: UIView!
//    
//    var kPreferredTextViewToKeyboardOffset: CGFloat = 0.0
//    var keyboardFrame: CGRect = CGRect.null
//    var keyboardIsShowing: Bool = false
//    
//    @IBOutlet var settingBtn: [UIButton]!
//    
//    
//    let inputText: NSArray = ["testing chat message outgoing, testing chat message outgoing, testing chat message outgoing.","testing chat message outgoing,","test","testing chat message outgoing, testing chat message outgoing, testing chat message outgoing, testing chat message outgoing, testing chat message outgoing.","testing chat message outgoing, testing chat message outgoing."]
//    
//    var getContact = [listcontact]()
//    
//    override func viewDidLoad()
//    {
//        super.viewDidLoad()
//        tableChat.delegate = self
//        tableChat.dataSource = self
//        menuTableView.delegate = self
//        menuTableView.dataSource = self
//        
//        
//        //-------------------------------------------Swipe-----------------------------------------------
//        
//        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture(gesture:)))
//        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
//        
//        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture(gesture:)))
//        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
//        
//        self.view.addGestureRecognizer(swipeRight)
//        self.view.addGestureRecognizer(swipeLeft)
//        //------------------------------------------------------------------------------------------------
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(TableGroupChat.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(TableGroupChat.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
//        
//        tableChat.keyboardDismissMode = .onDrag
//        
//        self.textViewChat.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
//        self.textViewChat.sizeToFit()
//        let lightestGrayColor: UIColor = UIColor( red: 224.0/255.0, green: 224.0/255.0, blue:224.0/255.0, alpha: 1.0 )
//        self.textViewChat.layer.borderColor = lightestGrayColor.cgColor
//        self.textViewChat.layer.borderWidth = 0.6
//        self.textViewChat.layer.cornerRadius = 6.0
//        self.textViewChat.clipsToBounds = true
//        self.textViewChat.layer.masksToBounds = true
//        self.textViewChat.isScrollEnabled = false
//        self.textViewChat.delegate = self
//        
//        tableChat.transform = CGAffineTransform(scaleX: 1, y: -1)
//        
//        print("The array get from contact :",getContact)
//    }
//    
//    @IBAction func dropDownBtn(_ sender: UIBarButtonItem)
//    {
//        if AppDelegate.menu_bool
//        {
//            showDropBtn()
//        }else{
//            closeDropBtn()
//        }
//        
//    }
//    
//    func showDropBtn()
//    {
//        settingBtn.forEach
//            {(button) in
//                UIView.animate(withDuration: 0.5, animations:
//                    {
//                        self.view.addSubview(self.dropMenu)
//                        button.isHidden = !button.isHidden
//                        self.view.layoutIfNeeded()
//                })
//        }
//        AppDelegate.menu_bool = false
//    }
//    
//    func closeDropBtn()
//    {
//        settingBtn.forEach
//            {(button) in
//                UIView.animate(withDuration: 0.5, animations:
//                    {
//                        button.isHidden = !button.isHidden
//                        self.view.layoutIfNeeded()
//                })
//                { (finished) in
//                    self.dropMenu.removeFromSuperview()
//                }
//        }
//        AppDelegate.menu_bool = true
//    }
//    
//    
//    //--------------------------------slidemenu function----------------------------------
//    @objc func respondToGesture(gesture : UISwipeGestureRecognizer)
//    {
//        switch gesture.direction
//        {
//        case UISwipeGestureRecognizer.Direction.right:
//            print("Right Swipe")
//            showMenu()
//            
//        case UISwipeGestureRecognizer.Direction.left:
//            print("Left Swipe")
//            closeMenu()
//            
//        default:
//            break
//        }
//    }
//    
//    
//    @IBAction func slideMenuBtn(_ sender: UIBarButtonItem)
//    {
//        
//        if AppDelegate.menu_bool
//        {
//            showMenu()
//        }else{
//            closeMenu()
//        }
//        
//    }
//    
//    func showMenu()
//    {
//        UIView.animate(withDuration: 0.3)
//        { ()->Void in
//            self.SlideMenu.frame = CGRect(x: 0, y: 40, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
//            
//            self.SlideMenu.backgroundColor = UIColor.black.withAlphaComponent(0.6)
//            self.view.addSubview(self.SlideMenu)
//            AppDelegate.menu_bool = false
//        }
//    }
//    
//    func closeMenu()
//    {
//        UIView.animate(withDuration: 0.3, animations:
//            { ()->Void in
//                self.SlideMenu.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 50, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
//        })
//        { (finished) in
//            self.SlideMenu.removeFromSuperview()
//        }
//        AppDelegate.menu_bool = true
//    }
//    
//    
//    
//    
//    
//    //---------------------------textview and keyboard function--------------------------
//    override func viewWillDisappear(_ animated: Bool)
//    {
//        super.viewWillDisappear(animated)
//        NotificationCenter.default.removeObserver(self)
//    }
//    
//    @objc func keyboardWillShow(notification: NSNotification)
//    {
//        self.keyboardIsShowing = true
//        
//        if let info = notification.userInfo {
//            self.keyboardFrame = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
//            self.arrangeViewOffsetFromKeyboard()
//        }
//    }
//    
//    @objc func keyboardWillHide(notification: NSNotification)
//    {
//        self.keyboardIsShowing = false
//        
//        self.returnViewToInitialFrame()
//    }
//    
//    func arrangeViewOffsetFromKeyboard()
//    {
//        let theApp: UIApplication = UIApplication.shared
//        let windowView: UIView? = theApp.delegate!.window!
//        
//        let textFieldLowerPoint: CGPoint = CGPoint(x: self.textViewPlaceholder!.frame.origin.x, y: self.textViewPlaceholder!.frame.origin.y + self.textViewPlaceholder!.frame.size.height)
//        
//        let convertedTextViewLowerPoint: CGPoint = self.view.convert(textFieldLowerPoint, to: windowView)
//        
//        let targetTextViewLowerPoint: CGPoint = CGPoint(x: self.textViewPlaceholder!.frame.origin.x, y: self.keyboardFrame.origin.y - kPreferredTextViewToKeyboardOffset)
//        
//        let targetPointOffset: CGFloat = targetTextViewLowerPoint.y - convertedTextViewLowerPoint.y
//        let adjustedViewFrameCenter: CGPoint = CGPoint(x: self.view.center.x, y: self.view.center.y + targetPointOffset)
//        
//        UIView.animate(withDuration: 0.2, animations:
//            {
//                self.view.center = adjustedViewFrameCenter
//        })
//    }
//    
//    func returnViewToInitialFrame()
//    {
//        let initialViewRect: CGRect = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
//        
//        if (!initialViewRect.equalTo(self.view.frame))
//        {
//            UIView.animate(withDuration: 0.2, animations: {
//                self.view.frame = initialViewRect
//            });
//        }
//    }
//    
//    func textViewDidBeginEditing(textView: UITextView)
//    {
//        
//        print("text view did begin editing\n")
//        
//        if(self.keyboardIsShowing)
//        {
//            self.arrangeViewOffsetFromKeyboard()
//        }
//    }
//    
//    func textViewDidEndEditing(textView: UITextView)
//    {
//        print("text view did end editing\n")
//        
//        textViewChat.resignFirstResponder()
//    }
//    
//    //    func handleChat()
//    //    {
//    //        let inputText = "testing chat message outgoing, testing chat message outgoing, testing chat message outgoing."
//    //
//    //        showOutgoingMessage(text: inputText)
//    //    }
//}
//
//
//
//extension TableGroupChat: UITableViewDataSource, UITableViewDelegate
//{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        if tableView == tableChat
//        {
//            return inputText.count
//        }else if tableView == menuTableView
//        {
//            return getContact.count
//        }
//        return Int()
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//    {
//        if tableView == tableChat
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! TableGroupChatCell
//            cell.transform = CGAffineTransform(scaleX: 1, y: -1)
//            cell.showOutgoingMessage(text: inputText[indexPath.row] as! String)
//            //            cell.showIncomingMessage(text: inputText[indexPath.row] as! String)
//            return cell
//        }
//        else if tableView == menuTableView
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "CellMenu", for: indexPath) as! SlideMenuTableViewCell
//            cell.menuLbl.text = getContact[indexPath.row].name
//            
//            return cell
//        }
//        
//        return UITableViewCell()
//    }
//    
//    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    //    {
//    //        return (tableView.cellForRow(at: indexPath)?.frame.height)!
//    //    }
//    
//}
//
//extension TableGroupChat : UITextViewDelegate
//{
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
//    {
//        if(self.textViewChat.contentSize.height < 130)
//        {
//            self.textViewChat.isScrollEnabled = false
//        }else
//        {
//            self.textViewChat.isScrollEnabled = true
//        }
//        return true
//    }
//}
