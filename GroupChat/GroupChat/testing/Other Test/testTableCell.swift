//
//  testTableCell.swift
//  GroupChat
//
//  Created by Elveen on 13/05/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import Foundation
import UIKit

class testTableCell : UITableViewCell
{
    @IBOutlet weak var Cview: chatBubbleView!
    @IBOutlet weak var CLabel: ChatBubbleLabel!
    
//    @IBOutlet weak var rightConst: NSLayoutConstraint!
//    @IBOutlet weak var leftConst: NSLayoutConstraint!
    
    var leftConstraint: NSLayoutConstraint!
    var allignRightConstraint: NSLayoutConstraint!
    var rightConstraint: NSLayoutConstraint!
    var allignLeftConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        CLabel.translatesAutoresizingMaskIntoConstraints = false
        Cview.translatesAutoresizingMaskIntoConstraints = false
        
        let topConsLabel = NSLayoutConstraint (item: CLabel, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: Cview, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 10)
        let bottomConsLabel = NSLayoutConstraint (item: CLabel, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: Cview, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -10)
        let leftConsLabel = NSLayoutConstraint (item: CLabel, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: Cview, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 10)
        let rightConsLabel = NSLayoutConstraint (item: CLabel, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: Cview, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -10)
        self.addConstraint(topConsLabel)
        self.addConstraint(bottomConsLabel)
        self.addConstraint(leftConsLabel)
        self.addConstraint(rightConsLabel)
        
        let topConsView = NSLayoutConstraint (item: Cview, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 10)
        let bottomConsView = NSLayoutConstraint (item: Cview, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -10)
        self.addConstraint(topConsView)
        self.addConstraint(bottomConsView)
        
        let heightConsLabel = NSLayoutConstraint (item: CLabel, attribute: .height, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
        self.addConstraint(heightConsLabel)
        
        let heightConsView = NSLayoutConstraint (item: Cview, attribute: .height, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
        self.addConstraint(heightConsView)
        

    
        leftConstraint = NSLayoutConstraint (item: Cview, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: self, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 15)
        
        allignRightConstraint = NSLayoutConstraint (item: Cview, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -15)
    
        rightConstraint = NSLayoutConstraint (item: Cview, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.lessThanOrEqual, toItem: self, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 15)
        
        allignLeftConstraint = NSLayoutConstraint (item: Cview, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 15)
        
        Cview.isIncoming = true
        
        if Cview.isIncoming == false
        {
            OutgoingAllignmentCorrection()
            
        }else
        {
            IncomingAllignmentCorrection()
        }

        CLabel.showIncomingMessage(text: "testing testing testin testing asdsd d adasdsad sadsadaas asdas dada adasdads asdadasds asdadsasddasd asdasdadad sadasdaas")
        
    }
    
    func IncomingAllignmentCorrection()
    {
        CLabel.textAlignment = .left
    
        self.addConstraint(allignLeftConstraint)
        self.addConstraint(rightConstraint)
    
        //self.setNeedsLayout()
    }

    func OutgoingAllignmentCorrection()
    {
        CLabel.textAlignment = .right
    
        self.addConstraint(leftConstraint)
        self.addConstraint(allignRightConstraint)
    
        //self.setNeedsLayout()
    }
}
