//
//  test.swift
//  GroupChat
//
//  Created by Elveen on 03/05/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import Foundation
import UIKit

class test : UIViewController
{
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var textView: ExTextView!
    @IBOutlet weak var table: UITableView!
    
    var kPreferredTextViewToKeyboardOffset: CGFloat = 0.0
    var keyboardFrame: CGRect = CGRect.null
    var keyboardIsShowing: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.placeholder(editText: "testing")
        table.delegate = self
        table.dataSource = self
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(test.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(test.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(notification: NSNotification)
    {
        self.keyboardIsShowing = true
        
        if let info = notification.userInfo {
            self.keyboardFrame = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            self.arrangeViewOffsetFromKeyboard()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification)
    {
        self.keyboardIsShowing = false
        
        self.returnViewToInitialFrame()
    }
    
    func arrangeViewOffsetFromKeyboard()
    {
        let theApp: UIApplication = UIApplication.shared
        let windowView: UIView? = theApp.delegate!.window!
        
        let textFieldLowerPoint: CGPoint = CGPoint(x: self.contentView!.frame.origin.x, y: self.contentView!.frame.origin.y + self.contentView!.frame.size.height)
        
        let convertedTextViewLowerPoint: CGPoint = self.view.convert(textFieldLowerPoint, to: windowView)
        
        let targetTextViewLowerPoint: CGPoint = CGPoint(x: self.contentView!.frame.origin.x, y: self.keyboardFrame.origin.y - kPreferredTextViewToKeyboardOffset)
        
        let targetPointOffset: CGFloat = targetTextViewLowerPoint.y - convertedTextViewLowerPoint.y
        let adjustedViewFrameCenter: CGPoint = CGPoint(x: self.view.center.x, y: self.view.center.y + targetPointOffset)
        
        UIView.animate(withDuration: 0.2, animations:
            {
                self.view.center = adjustedViewFrameCenter
        })
    }
    
    func returnViewToInitialFrame()
    {
        let initialViewRect: CGRect = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        if (!initialViewRect.equalTo(self.view.frame))
        {
            UIView.animate(withDuration: 0.2, animations: {
                self.view.frame = initialViewRect
            });
        }
    }
    
    func textViewDidBeginEditing(textView: UITextView)
    {
        
        print("text view did begin editing\n")
        
        if(self.keyboardIsShowing)
        {
            self.arrangeViewOffsetFromKeyboard()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView)
    {
        print("text view did end editing\n")
        
        textView.resignFirstResponder()
    }
    
}

extension test : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TestCell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
