//
//  SlideMenuTableViewCell.swift
//  GroupChat
//
//  Created by Elveen on 25/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import UIKit

class SlideMenuTableViewCell: UITableViewCell
{
    @IBOutlet weak var menuLbl: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
