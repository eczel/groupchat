//
//  TableGroupChat.swift
//  GroupChat
//
//  Created by Elveen on 22/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//
import Foundation
import UIKit


class TableGroupChat: UIViewController
{
    @IBOutlet weak var tableChat: UITableView!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var textViewChat: ExTextView!
    @IBOutlet weak var textViewPlaceholder: UIView!
    @IBOutlet weak var sendMessageBtn: UIButton!
    @IBOutlet var SlideMenu: UIView!
    @IBOutlet var dropMenu: UIView!

    var kPreferredTextViewToKeyboardOffset: CGFloat = 0.0
    var keyboardFrame: CGRect = CGRect.null
    var keyboardIsShowing: Bool = false

    @IBOutlet var settingBtn: [UIButton]!


    let inputText: NSArray = ["testing chat message outgoing, testing chat message outgoing, testing chat message outgoing.","testing chat message outgoing,","test","testing chat message outgoing, testing chat message outgoing, testing chat message outgoing, testing chat message outgoing, testing chat message outgoing.","testing chat message outgoing, testing chat message outgoing."]

    var getContact = [listcontact]()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        textViewChat.placeholder(editText: "Type Here...")
        tableChat.delegate = self
        tableChat.dataSource = self
        menuTableView.delegate = self
        menuTableView.dataSource = self

        //-------------------------------------------Swipe-----------------------------------------------

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture(gesture:)))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left

        self.view.addGestureRecognizer(swipeRight)
        self.view.addGestureRecognizer(swipeLeft)
        //------------------------------------------------------------------------------------------------

        NotificationCenter.default.addObserver(self, selector: #selector(TableGroupChat.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(TableGroupChat.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)

        tableChat.keyboardDismissMode = .onDrag

        tableChat.transform = CGAffineTransform(scaleX: 1, y: -1)

        print("The array get from contact :",getContact)
    }

    @IBAction func dropDownBtn(_ sender: UIBarButtonItem)
    {
        if AppDelegate.menu_bool
        {
            showDropBtn()
        }else{
            closeDropBtn()
        }
    }

    func showDropBtn()
    {
        settingBtn.forEach
        {(button) in
            UIView.animate(withDuration: 0.5, animations:
            {
                self.view.addSubview(self.dropMenu)
                button.isHidden = !button.isHidden
                self.view.layoutIfNeeded()
            })
        }
        AppDelegate.menu_bool = false
    }

    func closeDropBtn()
    {
        settingBtn.forEach
        {(button) in
            UIView.animate(withDuration: 0.5, animations:
            {
                button.isHidden = !button.isHidden
                self.view.layoutIfNeeded()
            })
            { (finished) in
                self.dropMenu.removeFromSuperview()
            }
        }
        AppDelegate.menu_bool = true
    }


    //--------------------------------slidemenu function----------------------------------
    @objc func respondToGesture(gesture : UISwipeGestureRecognizer)
    {
        switch gesture.direction
        {
        case UISwipeGestureRecognizer.Direction.right:
            print("Right Swipe")
            showMenu()

        case UISwipeGestureRecognizer.Direction.left:
            print("Left Swipe")
            closeMenu()

        default:
            break
        }
    }


    @IBAction func slideMenuBtn(_ sender: UIBarButtonItem)
    {
        if AppDelegate.menu_bool
        {
            showMenu()
        }else{
            closeMenu()
        }
    }

    func showMenu()
    {
        UIView.animate(withDuration: 0.3)
        { ()->Void in
            self.SlideMenu.frame = CGRect(x: 0, y: 40, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)

            self.SlideMenu.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.view.addSubview(self.SlideMenu)
            AppDelegate.menu_bool = false
        }
    }

    func closeMenu()
    {
        UIView.animate(withDuration: 0.3, animations:
        { ()->Void in
            self.SlideMenu.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 50, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        })
        { (finished) in
            self.SlideMenu.removeFromSuperview()
        }
        AppDelegate.menu_bool = true
    }





    //---------------------------textview and keyboard function--------------------------
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    @objc func keyboardWillShow(notification: NSNotification)
    {
        self.keyboardIsShowing = true

        if let info = notification.userInfo
        {
            self.keyboardFrame = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            self.arrangeViewOffsetFromKeyboard()
        }
    }

    @objc func keyboardWillHide(notification: NSNotification)
    {
        self.keyboardIsShowing = false

        self.returnViewToInitialFrame()
    }

    func arrangeViewOffsetFromKeyboard()
    {
        let theApp: UIApplication = UIApplication.shared
        let windowView: UIView? = theApp.delegate!.window!

        let textFieldLowerPoint: CGPoint = CGPoint(x: self.textViewPlaceholder!.frame.origin.x, y: self.textViewPlaceholder!.frame.origin.y + self.textViewPlaceholder!.frame.size.height)

        let convertedTextViewLowerPoint: CGPoint = self.view.convert(textFieldLowerPoint, to: windowView)

        let targetTextViewLowerPoint: CGPoint = CGPoint(x: self.textViewPlaceholder!.frame.origin.x, y: self.keyboardFrame.origin.y - kPreferredTextViewToKeyboardOffset)

        let targetPointOffset: CGFloat = targetTextViewLowerPoint.y - convertedTextViewLowerPoint.y
        let adjustedViewFrameCenter: CGPoint = CGPoint(x: self.view.center.x, y: self.view.center.y + targetPointOffset)

        UIView.animate(withDuration: 0.2, animations:
        {
            self.view.center = adjustedViewFrameCenter
        })
    }

    func returnViewToInitialFrame()
    {
        let initialViewRect: CGRect = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)

        if (!initialViewRect.equalTo(self.view.frame))
        {
            UIView.animate(withDuration: 0.2, animations:
            {
                self.view.frame = initialViewRect
            });
        }
    }

    func textViewDidBeginEditing(textView: UITextView)
    {
        print("text view did begin editing\n")

        if(self.keyboardIsShowing)
        {
            self.arrangeViewOffsetFromKeyboard()
        }
    }

    func textViewDidEndEditing(textView: UITextView)
    {
        print("text view did end editing\n")

        textViewChat.resignFirstResponder()
    }

    //    func handleChat()
    //    {
    //        let inputText = "testing chat message outgoing, testing chat message outgoing, testing chat message outgoing."
    //
    //        showOutgoingMessage(text: inputText)
    //    }
}



extension TableGroupChat: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tableChat
        {
            return inputText.count
        }else if tableView == menuTableView
        {
            return getContact.count
        }
        return Int()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tableChat
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! TableGroupChatCell
            cell.transform = CGAffineTransform(scaleX: 1, y: -1)
            cell.showOutgoingMessage(text: inputText[indexPath.row] as! String)
            //            cell.showIncomingMessage(text: inputText[indexPath.row] as! String)
            return cell
        }
        else if tableView == menuTableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellMenu", for: indexPath) as! SlideMenuTableViewCell
            cell.menuLbl.text = getContact[indexPath.row].name

            return cell
        }
        return UITableViewCell()
    }

    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    //    {
    //        return (tableView.cellForRow(at: indexPath)?.frame.height)!
    //    }

}







//---------------setting button--------------------
//    @IBAction func groupSetting(_ sender: Any)
//    {
//        settingBtn.forEach
//            {(button) in
//                UIView.animate(withDuration: 0.3, animations:
//                    {
//                        button.isHidden = !button.isHidden
//                        //self.view.layoutIfNeeded()
//                })
//        }
//    }
//
//    enum Setting : String
//    {
//        // the label of the button has empty space infont
//        case editProfile = " Edit Profile"
//        case qrCode = " QR Code"
//        case businessAccount = " Business Account"
//        case setting = " Settings"
//    }
//
//    @IBAction func settingTapped(_ sender: UIButton)
//    {
//        guard let title = sender.currentTitle,
//            let menu = Setting (rawValue: title)
//            else
//        {
//            return
//        }
//
//        switch menu
//        {
//        case .editProfile:
//            print("edit profile")
//
//        case .qrCode:
//            print("QR code")
//
//        case .businessAccount:
//            print("Business")
//
//        case .setting:
//            print("Setting")
//
//        default:
//            print("No such option")
//        }
//    }











//        let amountOfLinesToBeShown: CGFloat = 6
//        let maxHeight: CGFloat = textViewChat.font!.lineHeight * amountOfLinesToBeShown
//
//        self.textViewChat.sizeThatFits(CGSize(width: textViewChat.frame.size.width, height: maxHeight))
//
//
//    func textViewDidChange(textView: UITextView)
//    {
//        print("text view did change\n")
//        let textViewFixedWidth: CGFloat = self.textViewChat.frame.size.width
//        let newSize: CGSize = self.textViewChat.sizeThatFits(CGSize(width: textViewFixedWidth, height: CGFloat(MAXFLOAT)))
//        var newFrame: CGRect = self.textViewChat.frame
//        //
//        var textViewYPosition = self.textViewChat.frame.origin.y
//        let heightDifference = self.textViewChat.frame.height - newSize.height
//        //
//        if (abs(heightDifference) > 5) {
//            newFrame.size = CGSize(width: fmax(newSize.width, textViewFixedWidth), height: newSize.height)
//            newFrame.offsetBy(dx: 0.0, dy: heightDifference)
//            //
//            updateParentView(heightDifference: heightDifference)
//        }
//        self.textViewChat.frame = newFrame
//    }
//
//    func updateParentView(heightDifference: CGFloat)
//    {
//        //
//        var newContainerViewFrame: CGRect = self.textViewPlaceholder.frame
//        //
//        let containerViewHeight = self.textViewPlaceholder.frame.size.height
//        print("container view height: \(containerViewHeight)\n")
//        //
//        let newContainerViewHeight = containerViewHeight + heightDifference
//        print("new container view height: \(newContainerViewHeight)\n")
//        //
//        let containerViewHeightDifference = containerViewHeight - newContainerViewHeight
//        print("container view height difference: \(containerViewHeightDifference)\n")
//        //
//        newContainerViewFrame.size = CGSize(width: self.textViewPlaceholder.frame.size.width, height: newContainerViewHeight)
//        //
//        newContainerViewFrame.offsetBy(dx: 0.0, dy: containerViewHeightDifference)
//        //
//        newContainerViewFrame.origin.y - containerViewHeightDifference
//        self.textViewPlaceholder.frame = newContainerViewFrame
//    }
