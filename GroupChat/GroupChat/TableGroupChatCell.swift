//
//  TableGroupChatCell.swift
//  GroupChat
//
//  Created by Elveen on 22/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import Foundation
import UIKit

class TableGroupChatCell: UITableViewCell
{
    @IBOutlet weak var chatDate: UILabel!
    @IBOutlet weak var chatName: UILabel!
    @IBOutlet weak var chatMessages: UILabel!
    @IBOutlet weak var chatImage: UIImageView!
    @IBOutlet weak var chatStatus: UILabel!
    @IBOutlet weak var chatView: UIView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func showOutgoingMessage(text: String)
    {
        chatMessages.numberOfLines = 0
        chatMessages.font = UIFont.systemFont(ofSize: 13)
        chatMessages.textColor = .black
        chatMessages.text = text
        
        let constraintRect = CGSize(width: chatView.frame.width,
                                    height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [.font: chatMessages.font],
                                            context: nil)
        chatMessages.frame.size = CGSize(width: ceil(boundingBox.width),
                                  height: ceil(boundingBox.height))
        
        let bubbleSize = CGSize(width: chatMessages.frame.width + 15,
                                height: chatMessages.frame.height + 10)
        
        let bubbleView = chatBubble()
        bubbleView.frame.size = bubbleSize
        bubbleView.backgroundColor = .clear
        chatView.addSubview(bubbleView)
 //       chatMessages.addSubview(bubbleView)
        
        chatView.translatesAutoresizingMaskIntoConstraints = false
        chatView.leadingAnchor.constraint(equalTo: chatMessages.leadingAnchor).isActive = true
//        chatView.trailingAnchor.constraint(equalTo: chatMessages.trailingAnchor).isActive = true
        chatView.topAnchor.constraint(equalTo: chatMessages.topAnchor).isActive = true
        chatView.bottomAnchor.constraint(equalTo: chatMessages.bottomAnchor).isActive = true
        
//        chatName.leadingAnchor.constraint(equalTo: chatMessages.leadingAnchor).isActive = true
//        chatDate.leadingAnchor.constraint(equalTo: chatMessages.leadingAnchor).isActive = true

        chatView.addSubview(chatMessages)
        
        
    }
    
    func showIncomingMessage(text: String)
    {
        chatMessages.numberOfLines = 0
        chatMessages.font = UIFont.systemFont(ofSize: 13)
        chatMessages.textColor = .black
        chatMessages.text = text
    
        let constraintRect = CGSize(width: chatView.frame.width,
                                        height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect,
                                                options: .usesLineFragmentOrigin,
                                                attributes: [.font: chatMessages.font],
                                                context: nil)
        chatMessages.frame.size = CGSize(width: ceil(boundingBox.width),
                                      height: ceil(boundingBox.height))
    
        let bubbleSize = CGSize(width: chatMessages.frame.width,
                                height: chatMessages.frame.height)
    
        let bubbleView = chatBubble()
        bubbleView.isIncoming = true
        bubbleView.frame.size = bubbleSize
        bubbleView.backgroundColor = .clear
        chatView.addSubview(bubbleView)
        
        chatView.translatesAutoresizingMaskIntoConstraints = false
        chatView.leadingAnchor.constraint(equalTo: chatMessages.leadingAnchor).isActive = true
//        chatView.trailingAnchor.constraint(equalTo: chatMessages.trailingAnchor).isActive = true
        chatView.topAnchor.constraint(equalTo: chatMessages.topAnchor).isActive = true
        chatView.bottomAnchor.constraint(equalTo: chatMessages.bottomAnchor).isActive = true
    
//        chatView.center = chatView.center
        chatView.addSubview(chatMessages)
    }
    
}














//    func showIncomingImage()
//    {
//        let width: CGFloat = 0.66 * chatView.frame.width
//        let height: CGFloat = width / 0.75
//
//        let maskView = chatBubble()
//        maskView.isIncoming = true
//        maskView.backgroundColor = .clear
//        maskView.frame.size = CGSize(width: width, height: height)
//
//        let imageView = UIImageView(image: UIImage(named: "success"))
//        imageView.frame.size = CGSize(width: width, height: height)
//        imageView.center = chatView.center
//        imageView.contentMode = .scaleAspectFill
//        imageView.clipsToBounds = true
//        imageView.mask = maskView
//
//        chatView.addSubview(imageView)
//    }
//
//    func showOutgoingImage()
//    {
//        let width: CGFloat = 0.66 * chatView.frame.width
//        let height: CGFloat = width / 0.75
//
//        let maskView = chatBubble()
//        maskView.backgroundColor = .gray
//        maskView.frame.size = CGSize(width: width, height: height)
//
//        let imageView = UIImageView(image: UIImage(named: "success"))
//        imageView.frame.size = CGSize(width: width, height: height)
//        imageView.center = chatView.center
//        imageView.contentMode = .scaleAspectFill
//        imageView.clipsToBounds = true
//        imageView.mask = maskView
//
//        chatView.addSubview(imageView)
//    }
